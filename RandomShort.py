#Alumno: Mario Cobo Martinez (m.cobom.2019)
import random
import webapp

formulario   = """
                <form name="formulario" action="" method="POST">
                <p>"Url: <input type="text" name="contenido"> "</p>
                <input type="submit" value="Enviar">
                </form>"""


class randomshort(webapp.webapp):
    urls = {}

    def parse(self, received):
        recibido = received.decode()
        metodo = recibido.split(' ')[0]
        recurso = recibido.split(' ')[1]
        if metodo == "POST":
            cuerpo = recibido.split('\r\n\r\n')[1]
        else:
            cuerpo = None

        return metodo, recurso, cuerpo

    def process(self, analyzed):
        metodo, recurso, cuerpo = analyzed

        if metodo == "GET":
            if recurso == "/":
                httpCode = "200 OK"
                htmlBody = "<html><body>" + "Estas son las URLS guardadas: " + str(self.urls) +"\n"+ formulario + "</body></html>"
            else:
                if recurso in self.urls:
                    #url = self.urls[recurso]
                    httpCode = "200 OK"
                    htmlBody = "<html><body> Vamos al recurso: " + recurso + "..." \
                           + '<meta http-equiv="refresh" content="2;url=' + (self.urls[recurso]) + \
                           '">' "</body></html></body></html>"
                else:
                    httpCode = "404 Not Found"
                    htmlBody = "Not Found"
        elif metodo == 'POST':
            url = cuerpo.split('contenido=')[1]
            print(url)#traza
            rand = random.randint(1,1000)
            short = "/"+str(rand)
            sorti = "http://localhost:1234"+ short
            print(short)#traza
            print(sorti)

            if url == "":
                httpCode = "404 Not Found"
                htmlBody = "Not Found"
            else:
                if not url.startswith('http://') or not url.startswith('https://'):
                    url = "https://" + url

                self.urls[short] = url
                httpCode = "200 OK"
                htmlBody = f"<html><body>URL Original: <a href='{url}'>{url}</a>" \
                           f"<br>URL Acortada: <a href='{url}'>http://localhost:1234{short}</a></body></html>"

        else:
            httpCode = "404 Not Found"
            htmlBody = "Not Found"

        return httpCode, htmlBody


if __name__ == "__main__":
    testWebApp = randomshort("localhost", 1234)
